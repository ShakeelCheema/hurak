<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    use HasFactory;

    public function box()
    {
        return $this->belongsTo('App\Models\Box','box_color_id');
    }
}
