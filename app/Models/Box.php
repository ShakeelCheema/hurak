<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    use HasFactory;

    public function color()
    {
        return $this->hasOne('App\Models\Color','id', 'box_color_id');
    }
}
