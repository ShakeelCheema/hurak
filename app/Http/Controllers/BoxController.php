<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\Box;
use App\Http\Requests\StoreBoxRequest;
use App\Http\Requests\UpdateBoxRequest;
use App\Models\Color;

class BoxController extends Controller
{
    public function makeBox(){
        $boxes = Box::all();
        $boxes_count = $boxes->count();
        // if($arg == 'makeboxes'){
            if($boxes_count < 16 ){
                
                if($boxes_count == 0){
                    $boxes_count = 1;
                }
                   
                for($i = 1; $i<= $boxes_count; $i++){
                    $color = Color::inRandomOrder()->first();

                    $box = new Box();
                    $box->box_name      = $color->name.'_box';
                    $box->height        = '40px';
                    $box->width         = '100px';
                    $box->box_color_id  = $color->id;

                    $box->save();

                }
            }
        // }

        $boxes = Box::with('color')->get();

        $data = [
            'boxes'     => $boxes,
            'box_count' => $boxes_count
        ];

        return ResponseHelper::buildResponse(true, $data , 'success', '200');
    }

    public function shuffleAndSort($arg){
        $boxes = Box::all();
        $boxes_count = $boxes->count();

        if($arg == 'sort'){
            $boxes = Box::with('color')->orderBy('box_color_id')->get();
        }
        else{
            $boxes = Box::with('color')->inRandomOrder()->get();
        }

        $data = [
            'boxes'     => $boxes,
            'box_count' => $boxes_count
        ];

        return ResponseHelper::buildResponse(true, $data , 'success', '200');
    }

    public function sendmail(){
        $user_mail = "shakeeliqbal2551@gmail.com";
        $subject = '1st Task Done';
        $text = 'Shakeel Iqbal Cheema';
        HelperController::taskNotification($user_mail,$subject,$text);
    }
}
