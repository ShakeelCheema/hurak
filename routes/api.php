<?php

use App\Http\Controllers\BoxController;
use App\Http\Controllers\ColorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//get all colors
Route::get('/get/colors', [ColorController::class, 'index'])->name('colors');

//make boxes
Route::get('/make/box', [BoxController::class, 'makeBox'])->name('make.boxes');

//send mail
Route::get('/send/mail', [BoxController::class, 'sendmail'])->name('send.mail');

//shuffle and sort boxex
Route::get('/shuffle/sort/{shuffle_sort}', [BoxController::class, 'shuffleAndSort'])->name('shuffle.sort');


